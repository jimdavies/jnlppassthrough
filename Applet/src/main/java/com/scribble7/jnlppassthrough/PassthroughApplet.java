package com.scribble7.jnlppassthrough;

import javax.swing.*;
import java.applet.Applet;
import java.awt.*;
import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.net.HttpCookie;
import java.net.HttpURLConnection;
import java.net.URL;
import java.util.ArrayList;
import java.util.List;

import static javax.swing.SwingUtilities.invokeLater;

/**
 * Created by Jim Davies on 31/01/2014 06:31.
 *
 */
public class PassthroughApplet extends Applet {
	private volatile boolean running;
	private List<HttpCookie> cookies = new ArrayList<>();
	private JLabel status;
	private JLabel data;

	@Override
	public void init() {
		setupParameters();
		invokeLater(new Runnable() {
			@Override
			public void run() {
				setupUi();
			}
		});
	}

	private void setupParameters() {
		String keys = getParameter("cookieKeys");
		for(String key : keys.split(","))
			if(key != null && !key.isEmpty())
				cookies.add(new HttpCookie(key, getParameter(key)));
	}

	private void setupUi() {
		status = new JLabel("Dead");
		data = new JLabel("Nothing received");
		setLayout(new GridLayout(2, 1));
		add(status);
		add(data);
	}

	@Override
	public void start() {
		running = true;
		new Thread() {
			@Override
			public void run() {
				while(running) {
					status.setText("Sleeping");
					synchronized (this) {
						try {
							this.wait(1000);
						} catch (InterruptedException e) {
							setStatus("Sleeping interrupted");
						}
					}
					try {
						setStatus("Initialising");
						URL docBase = getDocumentBase();
						URL dataUrl = new URL(docBase, "data");
						setStatus("Opening connection");
						HttpURLConnection connection = (HttpURLConnection) dataUrl.openConnection();
						for(HttpCookie cookie : cookies)
							connection.setRequestProperty("Cookie", cookie.toString());
						BufferedReader reader = new BufferedReader(new InputStreamReader(connection.getInputStream()));
						setStatus("Reading data");
						String serviceData = reader.readLine();
						setData("Received: " + serviceData);
						setStatus("Closing connection");
						reader.close();
						setStatus("Request complete");
					} catch (IOException e) {
						setStatus("Error reading");
						setData(e.getMessage());
					}
				}
			}
		}.start();
	}

	private void setStatus(final String statusString) {
		invokeLater(new Runnable() {
			@Override
			public void run() {
				status.setText(statusString);
			}
		});
	}

	private void setData(final String dataString) {
		invokeLater(new Runnable() {
			@Override
			public void run() {
				data.setText(dataString);
			}
		});
	}

	@Override
	public void stop() {
		running = false;
	}
}
