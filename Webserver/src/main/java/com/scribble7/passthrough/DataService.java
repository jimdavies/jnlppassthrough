package com.scribble7.passthrough;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;

/**
 * Created by Jim Davies on 31/01/14 07:04.
 *
 */
public class DataService extends HttpServlet {
	@Override
	protected void doGet(HttpServletRequest httpServletRequest, HttpServletResponse res) throws ServletException, IOException {
		res.getWriter().println("Data");
	}
}
