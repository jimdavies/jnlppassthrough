package com.scribble7.passthrough;

import java.util.Collection;
import java.util.Map;

/**
 * Created by Jim Davies on 01/02/14 14:42.
 *
 */
public class Functions {
	public static String[] toStringArray(Collection<?> objects) {
		String[] strings = new String[objects.size()];
		int idx = 0;
		for(Object object : objects)
			strings[idx++] = String.valueOf(object);
		return strings;
	}

	public static String[] keysToStringArray(Map<?, ?> map) {
		return toStringArray(map.keySet());
	}

	public static String[] valuesToStringArray(Map<?, ?> map) {
		return toStringArray(map.values());
	}
}
