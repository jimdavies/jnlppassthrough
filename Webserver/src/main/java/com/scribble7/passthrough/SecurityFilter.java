package com.scribble7.passthrough;

import javax.servlet.*;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import java.io.IOException;

/**
 * Created by Jim Davies on 01/02/14 07:54.
 *
 */
public class SecurityFilter implements Filter {
	public void doFilter(ServletRequest servletRequest, ServletResponse servletResponse, FilterChain chain) throws ServletException, IOException {
		HttpServletRequest req = (HttpServletRequest) servletRequest;
		HttpServletResponse res = (HttpServletResponse) servletResponse;
		if(req.getSession(false) != null)
			chain.doFilter(req, res);
		else {
			res.setStatus(HttpServletResponse.SC_FORBIDDEN);
			res.getWriter().println("Not authenticated.");
		}
	}

	public void destroy() {}
	public void init(FilterConfig config) throws ServletException {}
}
